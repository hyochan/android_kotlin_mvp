package com.duchan.mobile.mvp_sample.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.duchan.mobile.mvp_sample.R
import com.duchan.mobile.mvp_sample.common.mvp.BaseMvpFragment
import com.duchan.mobile.mvp_sample.common.mvp.MvpPresenter
import com.duchan.mobile.mvp_sample.common.mvp.MvpView

abstract class CommonFragment<in V : MvpView, out P : MvpPresenter<V>> : BaseMvpFragment<V, P>() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater?.inflate(R.layout.content_main, container, false)
        return view
    }

    override fun attachView(view: V) {
    }

    override fun detachView() {
    }
}