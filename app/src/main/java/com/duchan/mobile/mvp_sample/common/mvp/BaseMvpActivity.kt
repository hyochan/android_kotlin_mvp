package com.duchan.mobile.mvp_sample.common.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseMvpActivity<in V : MvpView, out P : MvpPresenter<V>> : AppCompatActivity(), MvpView, MvpPresenter<V> {
    private var presenter : P? = null

    /**
     * Creates a new presenter instance. This method will be called from
     * [.onCreate]
     *
     * @return The [MvpPresenter] for this view
     */
    abstract fun createPresenter(): P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onPreAttached()
        presenter = createPresenter()
        attachView(getMvpView())
    }

    override fun onDestroy() {
        super.onDestroy()
        detachView()
    }

    fun getPresenter(): P? {
        if (presenter == null) {
            presenter = createPresenter()
        }
        return presenter
    }

    /**
     * Allow sub class to do some initialization before attaching to presenter
     * This is the first place to run user specific logic for MvpActivity,
     * which is called right after super.onCreate(Bundle)
     */
    protected fun onPreAttached() {
        // hooking method
    }

    private fun getMvpView(): V {
        return this as V
    }
}