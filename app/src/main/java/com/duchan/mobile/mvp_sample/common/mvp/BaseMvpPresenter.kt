package com.duchan.mobile.mvp_sample.common.mvp

import android.support.annotation.NonNull

abstract class BaseMvpPresenter<in VIEW : MvpView, MODEL> : MvpPresenter<VIEW> {
    private var model: MODEL = createModel()

    fun setModel(model: MODEL) {
        this.model = model
    }

    @NonNull
    fun model(): MODEL {
        return model
    }

    override fun attachView(view: VIEW) {
    }

    override fun detachView() {
    }

    protected abstract fun createModel(): MODEL
}