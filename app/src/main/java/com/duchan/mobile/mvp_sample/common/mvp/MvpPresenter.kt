package com.duchan.mobile.mvp_sample.common.mvp

interface MvpPresenter<in VIEW : MvpView> {

    /**
     * View Attach.
     */
    fun attachView(view: VIEW)

    /**
     * View detach
     */
    fun detachView()
}