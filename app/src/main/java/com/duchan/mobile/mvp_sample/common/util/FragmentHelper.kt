package com.duchan.mobile.mvp_sample.common.util

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

class FragmentHelper(private val context: Context, private val fragmentResId: Int) {
    private var fragmentManager: FragmentManager = (this.context as FragmentActivity).supportFragmentManager

    fun replaceFragment(fragment: Fragment, tag: String) {
        replaceFragment(fragmentResId, fragment, tag, 0, 0, 0, 0)
    }

    @JvmOverloads
    fun replaceFragment(fragment: Fragment, enterAniResId: Int = 0, popAniResId: Int = 0) {
        replaceFragment(fragment, enterAniResId, 0, 0, popAniResId)
    }

    fun replaceFragment(fragment: Fragment, enterAniResId: Int, exitAniResId: Int, popEnterAniResId: Int, popExitAniResId: Int) {
        replaceFragment(fragmentResId, fragment, null, enterAniResId, exitAniResId, popEnterAniResId, popExitAniResId)
    }

    fun replaceFragment(fragment: Fragment, tag: String, enterAniResId: Int, exitAniResId: Int, popEnterAniResId: Int, popExitAniResId: Int) {
        replaceFragment(fragmentResId, fragment, tag, enterAniResId, exitAniResId, popEnterAniResId, popExitAniResId)
    }

    @JvmOverloads
    fun replaceFragment(fragmentResId: Int, fragment: Fragment, tag: String? = null, enterAniResId: Int = 0, exitAniResId: Int = 0, popEnterAniResId: Int = 0,
                        popExitAniResId: Int = 0) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(enterAniResId, exitAniResId, popEnterAniResId, popExitAniResId)
                .replace(fragmentResId, fragment, tag ?: fragment.javaClass.simpleName)
                .addToBackStack(tag)
                .commit()
    }

    fun addFragment(fragmentResId: Int, fragment: Fragment, tag: String?,
                    enterAniResId: Int, exitAniResId: Int, popEnterAniResId: Int, popExitAniResId: Int) {
        // we will only add one fragment for same tag
        if (findFragmentByTag(tag) != null) {
            return
        }
        fragmentManager.beginTransaction()
                .setCustomAnimations(enterAniResId, exitAniResId, popEnterAniResId, popExitAniResId)
                .add(fragmentResId, fragment, tag ?: fragment.javaClass.simpleName)
                .addToBackStack(tag)
                .commit()
    }

    fun removeFragment() {
        fragmentManager.popBackStack()
    }

    fun hasRemovableFragment(): Boolean {
        return fragmentManager.backStackEntryCount > 1
    }

    fun findFragmentById(fragmentResId: Int): Fragment {
        return fragmentManager.findFragmentById(fragmentResId)
    }

    private fun findFragmentByTag(tag: String?): Fragment? {
        return fragmentManager.findFragmentByTag(tag)
    }

    fun removeFragment(fragmentResId: Int) {
        val fragment = fragmentManager.findFragmentById(fragmentResId)
        if (fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).commit()
        }
    }

    fun popFragment(tag: String) {
        fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun showFragment(fragment: Fragment?) {
        if (fragment != null) {
            fragmentManager.beginTransaction().show(fragment).commit()
        }
    }

    fun hideFragment(fragment: Fragment?) {
        if (fragment != null) {
            fragmentManager.beginTransaction().hide(fragment).commit()
        }
    }
}