package com.duchan.mobile.mvp_sample.main.presenter

import com.duchan.mobile.mvp_sample.common.mvp.BaseMvpPresenter
import com.duchan.mobile.mvp_sample.main.model.HomeModel
import com.duchan.mobile.mvp_sample.main.view.HomeView

class HomePresenter : BaseMvpPresenter<HomeView, HomeModel>() {

    override fun createModel(): HomeModel {
        return HomeModel()
    }
}