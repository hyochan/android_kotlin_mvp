package com.duchan.mobile.mvp_sample.main.presenter

import com.duchan.mobile.mvp_sample.common.mvp.BaseMvpPresenter
import com.duchan.mobile.mvp_sample.main.model.MainModel
import com.duchan.mobile.mvp_sample.main.view.MainView

class MainPresenter : BaseMvpPresenter<MainView, MainModel>() {

    override fun createModel(): MainModel {
        return MainModel()
    }
}