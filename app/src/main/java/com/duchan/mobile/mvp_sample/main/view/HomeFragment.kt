package com.duchan.mobile.mvp_sample.main.view

import com.duchan.mobile.mvp_sample.common.CommonFragment
import com.duchan.mobile.mvp_sample.main.presenter.HomePresenter

class HomeFragment : CommonFragment<HomeView, HomePresenter>(), HomeView {

    override fun createPresenter(): HomePresenter {
        return HomePresenter()
    }
}